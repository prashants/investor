#!/usr/bin/env python
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
#
# Author : Prashant Shah <pshah.mumbai@gmail.com>

# Begin code

import csv
import logging
import os
import sqlite3
import sys

from datetime import datetime
from dateutil.parser import parse

print
print "Welcome to investor data importer module !"
print

logging.basicConfig(filename = 'investor.log', level = logging.DEBUG)

if len(sys.argv) != 2:
	print "Usage: python dataimport.py [EOD Filename].csv"
	sys.exit (1)

ipfile = sys.argv[1]

if not (os.path.exists(ipfile)) :
	print "Error: Import file does not exists."
	print
	sys.exit (1)

if not (os.path.isfile(ipfile)) :
	print "Error: Import file is invalid."
	print
	sys.exit (1)

filename, fileext = os.path.splitext(ipfile)

if (fileext != ".csv"):
	print "Error: Import file is not a valid csv file."
	print
	sys.exit (1)

sqlite_datafile = 'stockdata.sqlite'

if not (os.path.exists(sqlite_datafile)) :
	print "Error: stock data file does not exists. Please run the setup.py script first."
	print
	sys.exit (1)

# Initialize some variables
symbol_id = 0
eoddate_id = 0

# Connect to database
conn = sqlite3.connect('stockdata.sqlite')

c = conn.cursor()

# Extract EOD date from the filename
eod_date_str = filename[(len(filename) - 13):(len(filename)- 4)]
eod_date = datetime.strptime(eod_date_str, '%d%b%Y')

# Verify with user whether the EOD is correct before inserting the data
while True:
	sys.stdout.write("Is the EOD '" + eod_date_str + "' (" + eod_date.strftime("%Y-%m-%d") + ") correct [Y/N] ? ")
	choice = raw_input().lower()
	if choice == "n":
		print "No action taken. Thank you."
		conn.close()
		sys.exit (1)
	elif choice == "y":
		print "Importing " + ipfile + " ..."
		break
	else:
		print ("Please respond with 'Y' or 'N'")

# Search if date for same date is already inserted into the database
c.execute('SELECT * FROM eods WHERE date = ?', (eod_date_str,))
result = c.fetchone()
if result:
	logging.error("EOD data for %s already existing in database", eod_date_str)
	print "EOD data for", eod_date_str, "already existing in database"
	conn.close()
	sys.exit (1)
else:
	c.execute('INSERT INTO eods (date, eod_date) VALUES (?, ?)', (eod_date_str, eod_date.strftime("%Y-%m-%d")))

# Importing the data from EOD csv file
with open(ipfile) as csvfile:
	csvreader = csv.reader(csvfile, delimiter=',')
	for row in csvreader:

		# Skip first row of CSV because it contains title
		if (row[0] == "SYMBOL"):
			continue

		# Skip stocks that are not EQ
		if (row[1] != "EQ"):
			continue

		# Search of stock is present in the stocks master database
		c.execute('SELECT * FROM stocks WHERE symbol = ?', (row[0],))
		result = c.fetchone()

		if not result:
			# If stock does not exists in the stocks master database then add it
			# The older version of CSV data files does not contain a isin column hence this check
			if (len(row) == 12):
				c.execute('INSERT INTO stocks (symbol, isin) VALUES (?, "")', (row[0],))
			else:
				c.execute('INSERT INTO stocks (symbol, isin) VALUES (?, ?)', (row[0], row[12]))

			# Save the symbol id of the stock
			symbol_id = c.lastrowid
			logging.debug('Company added to stocks master : %s : %s', row[0], symbol_id)
		else:
			# Save the symbol id of the stock
			symbol_id = result[0]
			logging.debug('Company exists in stocks master : %s : %s', row[0], symbol_id)

		# Insert the stock data into the database
		c.execute('INSERT INTO stockdata (stock_id, eod_date, close , quantity) VALUES (?, ?, ?, ?)', (symbol_id, eod_date.strftime("%Y-%m-%d"), row[5], row[8]))
		logging.debug('Stock data added : %s : %s', row[0], symbol_id)

conn.commit()
conn.close()
print "All done !"
