#!/usr/bin/env python
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
#
# Author : Prashant Shah <pshah.mumbai@gmail.com>

# Begin code

# Create the initial database

import os
import sqlite3
import sys

print
print "Welcome to investor setup module !"
print

sqlite_datafile = 'stockdata.sqlite'

if (os.path.exists(sqlite_datafile)) :
	print "Error: stock data file already exists"
	print
	sys.exit (1)

conn = sqlite3.connect('stockdata.sqlite')

c = conn.cursor()

# Create stock master table
c.execute('''CREATE TABLE stocks (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, symbol VARCHAR(50) NOT NULL, isin VARCHAR(50) NOT NULL)''')

# Create EOD dates master table
c.execute('''CREATE TABLE eods (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, date VARCHAR(9) NOT NULL, eod_date DATE NOT NULL)''')

# Create stock stock data table
c.execute('''CREATE TABLE stockdata (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, stock_id INTEGER NOT NULL, eod_date DATE NOT NULL, close INTEGER NOT NULL, quantity INTEGER NOT NULL)''')

# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()

print "Setup completed !"
